#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <regex>

int main(int argc, char *argv[])
{

  //must have exactly 2 legit params
  if (argc != 3)
  {
    std::cout << "Wrong number of parameters." << std::endl;
    return 1;
  }

  std::ifstream input_file(argv[1]);
  if (!input_file.is_open())
  {
    std::cout << "Failed to open file: " << argv[1] << std::endl;
    return 1;
  }

  std::vector<std::pair<uint32_t, std::string>> vec;
  std::string buff;
  unsigned long long line = 0;

  while (std::getline(input_file, buff))
  {
    line++;

    //check if line is valid - 1 number and optional whitespaces around it
    std::regex single_number_on_line("^[[:space:]]*[0-9]+[[:space:]]*$");
    if (!std::regex_match(buff, single_number_on_line))
    {
      std::cout << "Line " << std::to_string(line) << ": invalid number format." << std::endl;
      return 1;
    }

    unsigned long num = std::stoul(buff, nullptr, 0);

    //check for conversion uint32_t overflow
    if (errno == ERANGE || num > UINT32_MAX)
    {
      std::cout << "Line " << std::to_string(line) << ": invalid number format." << std::endl;
      return 1;
    }

    vec.push_back(std::pair<uint32_t, std::string>((uint32_t)num, buff));
  }

  std::sort(vec.begin(), vec.end(), [](auto &left, auto &right) {
    return left.first < right.first;
  });

  std::ofstream output_file(argv[2]);

  //check if output_file opened/created successfully
  if (!output_file.is_open())
  {
    std::cout << "Failed to open file: " << argv[1] << std::endl;
    return 1;
  }

  for (auto num : vec)
  {
    output_file << num.second << std::endl;
  }

  return 0;
}