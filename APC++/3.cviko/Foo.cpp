//
// Created by kovko on 5.10.2016.
//

#include <iostream>
#include "Foo.h"

Foo::Foo()
    : m_i(543) {
  std::cout << "Foo" << std::endl;
}

Foo::~Foo() {
  std::cout << "~Foo" << std::endl;
}

const std::string &Foo::getM_s() const {
  return <#initializer#>;
}

void Foo::setM_s(const std::string &m_s) {

}
