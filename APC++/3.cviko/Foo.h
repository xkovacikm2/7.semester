//
// Created by kovko on 5.10.2016.
//

#ifndef INC_3_CVIKO_FOO_H
#define INC_3_CVIKO_FOO_H


class Foo {
public:
  Foo();
  ~Foo();
  int GetI() const {return this->m_i;};
  const std::string& GetString() const {return m_s;};

private:
  int m_i = 0;
  std::string m_s;
public:
  const std::string &getM_s() const;

  void setM_s(const std::string &m_s);
};


#endif //INC_3_CVIKO_FOO_H
