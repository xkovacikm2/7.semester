#include <iostream>
#include <memory>
#include "Foo.h"

int main() {
  auto p = std::make_unique<Foo>();
  return 0;
}