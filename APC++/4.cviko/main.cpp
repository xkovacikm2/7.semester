#include <iostream>
#include <memory>

using namespace std;

class A{
public:
  virtual ~A(){}
  virtual void print (){
    cout << "Hi from A" << endl;
  }
};

class B : public A {
public:
  ~B() override  {}
  void print() override {
    cout << "Hi from B" << endl;
  }
};

void f(A* a){
  const B* pb = dynamic_cast<B*>(a);
  a->print();
  if (pb == nullptr)
    cout << "A" << endl;
  else
    cout << "B" << endl;
}

void g(unique_ptr<A> a){
  a->print();
}

int main() {
  A a;
  f(&a);
  B b;
  f(&b);

  unique_ptr<A> c(new B);
  g(move(c));

  return 0;
}