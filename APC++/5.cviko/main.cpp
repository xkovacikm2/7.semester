#include <iostream>
#include <bits/stl_relops.h>

class BigInt{
  bool operator<(const BigInt &rhs);

  bool operator==(const BigInt &rhs);

  BigInt operator+=(const BigInt &rhs);

  BigInt operator+() const {
    return *this;
  }

  bool operator<=(const BigInt &rhs){
    return std::rel_ops::operator!=(*this, rhs);
  }

  BigInt operator+(BigInt lhs, const BigInt &rhs) const {
    lhs += rhs;
    return lhs;
  }
};

int main() {
  std::cout << "Hello, World!" << std::endl;
  return 0;
}