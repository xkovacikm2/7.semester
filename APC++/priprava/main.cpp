class A {
private:
  virtual void f() = 0;
};

class B : public A {
protected:
  virtual void f() override {}
};

class C : public B, private A {
public:
  virtual void g() { B::f(); }

private:
  virtual void f() override {}
};

int main() {
  C c;
  c.g();
  return 0;
}