//
// Created by kovko on 5.10.2016.
//

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <regex>
#include "Sudoku.h"

using namespace std;

Sudoku::Sudoku() {
  m_table = vector<vector<unsigned int>>(SUDOKU_TABLE_SIZE, vector<unsigned int>
      (SUDOKU_TABLE_SIZE, 0));
  m_solved_table = vector<vector<unsigned int>>(SUDOKU_TABLE_SIZE,
                                                vector<unsigned int>
                                                    (SUDOKU_TABLE_SIZE, 0));
}

Sudoku::Sudoku(const string &serialized) : Sudoku() {
  //check if there are SUDOKU_TABLE_SIZE sq numbers separated by
  // SUDOKU_TABLE_SIZE sq -1 separated by ,
  if (!regex_match(serialized, regex("^([0-9]+,){" + to_string(
      SUDOKU_TABLE_SIZE * SUDOKU_TABLE_SIZE - 1) + "}[0-9]+$"))) {
    return;
  }

  auto unchecked_values = this->split(serialized, ',');

  //check if there is parsed right amount of values
  if (unchecked_values.size() != SUDOKU_TABLE_SIZE * SUDOKU_TABLE_SIZE) {
    return;
  }

  auto itr = unchecked_values.begin();
  for (size_t i = 0; i < SUDOKU_TABLE_SIZE; i++) {
    for (size_t j = 0; j < SUDOKU_TABLE_SIZE; j++) {
      istringstream ss(*itr);
      uint32_t val = 0;
      ss >> val;
      //if value is out of range, use 0
      if (!number_in_range(val)) {
        val = 0;
      }
      this->m_table[i][j] = val;
      if (itr != unchecked_values.end()) {
        itr++;
      }
    }
  }
}

bool Sudoku::is_valid() const {
  auto rows = vector<vector<bool>>(SUDOKU_TABLE_SIZE,
                                   vector<bool>(SUDOKU_TABLE_SIZE, false));
  auto boxes = vector<vector<bool>>(SUDOKU_TABLE_SIZE, vector<bool>
      (SUDOKU_TABLE_SIZE, false));
  int box_size = (int) sqrt(SUDOKU_TABLE_SIZE);

  for (int i = 0; i < SUDOKU_TABLE_SIZE; i++) {
    auto col = vector<bool>(SUDOKU_TABLE_SIZE, false);
    for (int j = 0; j < SUDOKU_TABLE_SIZE; j++) {
      //not validating zero
      if (this->m_table[i][j] == 0) {
        continue;
      }

      if (col[this->m_table[i][j] - 1] || rows[j][this->m_table[i][j] - 1] ||
          boxes[(i / box_size) * box_size + (j / box_size)][
              this->m_table[i][j] - 1]) {
        return false;
      }
      col[this->m_table[i][j] - 1] = rows[j][this->m_table[i][j] - 1] =
      boxes[(i / box_size) * box_size + (j / box_size)][this->m_table[i][j] - 1]
          = true;
    }
  }

  return true;
}

bool Sudoku::solve() {
  do {
    ++(*this);
  } while (!this->is_solved() && this->m_state == SUDOKU_STATE::changed);
  return this->is_solved();
}

unsigned int Sudoku::operator()(size_t i, size_t j) const {
  return this->get_number(i, j);
}

unsigned int Sudoku::get_number(size_t i, size_t j) const {
  if (this->is_in_bounds(i, j)) {
    return this->m_table[i - 1][j - 1];
  }
  return 0;
}

bool Sudoku::set_number(size_t i, size_t j, uint32_t number) {
  if (this->is_in_bounds(i, j) && this->number_in_range(number)) {
    this->m_table[i - 1][j - 1] = number;
    this->refresh_solved();
    return true;
  }
  return false;
}

Sudoku &Sudoku::operator++() {
  this->m_state = SUDOKU_STATE::unchanged;
  if (this->is_valid()) {
    if (this->m_solved_table[0][0] != 0 || this->internal_solve()) {
      size_t i = 0;
      size_t j = 0;
      if (this->find_zero(&i, &j)) {
        this->m_table[i][j] = this->m_solved_table[i][j];
        this->m_state = SUDOKU_STATE::changed;
      }
    }
  }
  return *this;
}

string Sudoku::serialize() const {
  string s;
  for (auto &line:this->m_table) {
    for (auto &number:line) {
      s += to_string(number) + ",";
    }
  }
  s.pop_back();
  return s;
}

bool Sudoku::is_solved() {
  for (auto &i:this->m_table) {
    for (auto &j:i) {
      if (j == 0) {
        return false;
      }
    }
  }
  return true;
}

std::vector<std::string> Sudoku::split(const std::string &s, const char delim) {
  vector<string> vec;

  //moves from delimiter to delimiter and splits string
  for (size_t p = 0, q = 0; p != s.npos; p = q) {
    vec.push_back(s.substr(p + (p != 0), (q = s.find(delim, p + 1)) - p -
                                         (p != 0)));
  }

  return vec;
}

bool Sudoku::is_in_bounds(const size_t &i, const size_t &j) const {
  return (i > 0 && j > 0 && i <= SUDOKU_TABLE_SIZE && j <= SUDOKU_TABLE_SIZE);
}

bool Sudoku::number_in_range(const uint32_t &num) const {
  return num <= SUDOKU_TABLE_SIZE;
}

bool Sudoku::internal_solve() {
  if (this->m_solved_table[0][0] == 0) {
    this->copy_tables();
  }
  size_t i = 0;
  size_t j = 0;
  if (!this->find_internal_zero(&i, &j) && this->is_internal_valid()) {
    return true;
  }
  for (uint32_t val = 1; val <= SUDOKU_TABLE_SIZE; val++) {
    this->m_solved_table[i][j] = val;
    if (this->is_internal_valid()) {
      if (this->internal_solve()) {
        return true;
      }
    }
  }
  this->m_solved_table[i][j] = 0;
  return false;
}

void Sudoku::refresh_solved() {
  if (this->m_solved_table[0][0] == 0) {
    return;
  }

  for (auto &i:this->m_solved_table) {
    for (auto &j:i) {
      j = 0;
    }
  }
}

bool Sudoku::find_zero(size_t *y, size_t *x) {
  for (size_t i = 0; i < SUDOKU_TABLE_SIZE; i++) {
    for (size_t j = 0; j < SUDOKU_TABLE_SIZE; j++) {
      if (this->m_table[i][j] == 0) {
        *y = i;
        *x = j;
        return true;
      }
    }
  }
  return false;
}

bool Sudoku::find_internal_zero(size_t *y, size_t *x) {
  for (size_t i = 0; i < SUDOKU_TABLE_SIZE; i++) {
    for (size_t j = 0; j < SUDOKU_TABLE_SIZE; j++) {
      if (this->m_solved_table[i][j] == 0) {
        *y = i;
        *x = j;
        return true;
      }
    }
  }
  return false;
}

void Sudoku::copy_tables() {
  for (size_t i = 0; i < SUDOKU_TABLE_SIZE; i++) {
    for (size_t j = 0; j < SUDOKU_TABLE_SIZE; j++) {
      this->m_solved_table[i][j] = this->m_table[i][j];
    }
  }
}

ostream &operator<<(ostream &lhs, const Sudoku &rhs) {
  int sq_size = (int) sqrt(SUDOKU_TABLE_SIZE);
  for (size_t i = 0; i < SUDOKU_TABLE_SIZE; i++) {
    if (i % sq_size == 0) {
      lhs << "+";
      for (size_t k = 0; k < SUDOKU_TABLE_SIZE; k++) {
        lhs << "-";
      }
      lhs << "+\n";
    }

    for (size_t j = 0; j < SUDOKU_TABLE_SIZE; j++) {
      if (j % sq_size == 0) {
        lhs << "|";
      }
      lhs << rhs.m_table[i][j];
    }
    lhs << "|\n";
  }
  lhs << "+";
  for (size_t k = 0; k < SUDOKU_TABLE_SIZE; k++) {
    lhs << "-";
  }
  lhs << "+\n";
  return lhs;
}

bool Sudoku::is_internal_valid() const {
  auto rows = vector<vector<bool>>(SUDOKU_TABLE_SIZE,
                                   vector<bool>(SUDOKU_TABLE_SIZE, false));
  auto boxes = vector<vector<bool>>(SUDOKU_TABLE_SIZE, vector<bool>
      (SUDOKU_TABLE_SIZE, false));
  int box_size = (int) sqrt(SUDOKU_TABLE_SIZE);

  for (int i = 0; i < SUDOKU_TABLE_SIZE; i++) {
    auto col = vector<bool>(SUDOKU_TABLE_SIZE, false);
    for (int j = 0; j < SUDOKU_TABLE_SIZE; j++) {
      //not validating zero
      if (this->m_solved_table[i][j] == 0) {
        continue;
      }

      if (col[this->m_solved_table[i][j] - 1] ||
          rows[j][this->m_solved_table[i][j] - 1] ||
          boxes[(i / box_size) * box_size + (j / box_size)][
              this->m_solved_table[i][j] - 1]) {
        return false;
      }
      col[this->m_solved_table[i][j] - 1] =
      rows[j][this->m_solved_table[i][j] - 1] =
      boxes[(i / box_size) * box_size + (j / box_size)
      ][this->m_solved_table[i][j] - 1]
          = true;
    }
  }

  return true;
}