#pragma once

#include <string>
#include <ostream>
#include <vector>

#define SUDOKU_TABLE_SIZE 16

enum class SUDOKU_STATE {
    unchanged, changed
};

class Sudoku {
public:
    /**
    * Creates empty sudoku. 
    */
    Sudoku();

    /**
    * Creates sudoku from some text serialized state (created by 
    * call to serialize). It should detect corruption of serialized 
    * data (unexpected format) in that case empty sudoku is created.  
    */
    explicit Sudoku(const std::string &serialized);

    /**
    * Checks if current sudoku state doesn't violates the rules. 
    */
    bool is_valid() const;

    /**
    * Fill all blanks, returns true if the solution exists, 
    * otherwise false. 
    */
    bool solve();

    /**
    * The same as GetNumber(i, j).
    */
    uint32_t operator()(size_t i, size_t j) const;

    /**
    * Gets number on position i, j. Position is 1 based from
    * top left corner. Should return 0 if i or j is larger than
    * size of sudoku. Also it should return 0 if 
    * the square is blank. 
    */
    uint32_t get_number(size_t i, size_t j) const;

    /**
    * Sets number on position i, j. Position is 1 based from
    * top left corner. Should return false if i or j is larger than
    * size of sudoku. Or the number is out of range. 
    */
    bool set_number(size_t i, size_t j, uint32_t number);

    /**
    * Fill one empty square with correct solution. If sudoku is 
    * complete or no solution is possible ++ will have no effect. 
    */
    Sudoku &operator++();

    /**
    * Serialize sudoku do string is may not be pretty. Serialized 
    * state should work with appropriate constructor. 
    */
    std::string serialize() const;

private:
    /**
     * Model for sudoku
     */
    std::vector<std::vector<unsigned int>> m_table;

    /**
     * Internal solved table.
     */
    std::vector<std::vector<unsigned int>> m_solved_table;

    /**
     * Fills m_solved_table if possible.
     * @return
     */
    bool internal_solve();

    /**
     * Deletes internal solution, after action for state modifiers.
     */
    void refresh_solved();

    /**
     * Sets y and x to coordinates of first zero encounter.
     * @param y
     * @param x
     * @return true if zero was found, false otherwise.
     */
    bool find_zero(size_t *y, size_t *x);

    /**
     * Same as find zero but for internal table.
     * @param y
     * @param x
     * @return
     */
    bool find_internal_zero(size_t *y, size_t *x);

    /**
     * Copies content from m_table to m_solved_table.
     */
    void copy_tables();

    /**
     * Flag for operator++ and solve method
     */
    SUDOKU_STATE m_state = SUDOKU_STATE::unchanged;

    /**
     * @return true if all content of the table is nonzero, else false
     */
    bool is_solved();

    /**
     * @param index
     * @return true if index within bounds of the table, false otherwise
     */
    bool is_in_bounds(const size_t &i, const size_t &j) const;

    /**
     * @param num
     * @return true if number is in range for sudoku, false otherwise
     */
    bool number_in_range(const uint32_t &num) const;

    /**
     * Save as is_valid but for solved_table.
     * @return true if internal table is valid.
     */
    bool is_internal_valid() const;

    std::vector<std::string> split(const std::string &s, const char delim);

    /**
    * Prints Sudoku board to selected output stream.
    */
    friend std::ostream &operator<<(std::ostream &lhs, const Sudoku &rhs);
};



