#include <iostream>
#include "Sudoku.h"

using namespace std;

int main() {
  auto s = Sudoku();
  bool kek = s.is_valid();
  s.solve();
  cout << s;
  return 0;
}