//
// Created by kovko on 14.10.2016.
//

#include "Circle.h"

using namespace std;

Circle::Circle(const Point &c, const double &r) : m_r(r), m_c(c) {
}

void Circle::move(const double x_offset, const double y_offset) {
  this->m_c = Point(this->m_c.get_x() + x_offset, this->m_c.get_y() + y_offset);
}

Rect Circle::bound_box() const {
  Point a(this->m_c.get_x() - this->m_r, this->m_c.get_y() - this->m_r);
  Point b(this->m_c.get_x() + this->m_r, this->m_c.get_y() + this->m_r);
  return Rect(a, b);
}

unique_ptr<Shape> Circle::clone() {
  return std::move(unique_ptr<Shape>(new Circle(*this)));
}
