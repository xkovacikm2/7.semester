//
// Created by kovko on 14.10.2016.
//

#ifndef ZADANIE_3_CIRCLE_H
#define ZADANIE_3_CIRCLE_H

#include "Shape.h"
#include "Rect.h"

/**
 * Circle is defined by center point and radius.
 */
class Circle : public Shape {
public:
  Circle(const Point &c, const double &r);

  /**
   * Moves center by by given offset.
   * @param x_offset
   * @param y_offset
   */
  void move(const double x_offset, const double y_offset) override;

  /**
   * @return rectangle bound-boxing the circle.
   */
  Rect bound_box() const override;

  /**
   * Clones object because copy constructor is too mainstream.
   * @return
   */
  std::unique_ptr<Shape> clone() override;

  Circle() = delete;
  ~Circle() {}

private:
  Point m_c;
  const double m_r;
};


#endif //ZADANIE_3_CIRCLE_H
