//
// Created by kovko on 14.10.2016.
//

#include "Group.h"

using namespace std;

Rect Group::bound_box() const {
  if (this->m_shapes.empty()) {
    return Rect(Point(0, 0), Point(0, 0));
  }

  auto max_x = this->m_shapes[0]->bound_box().get_a().get_x();
  auto min_x = max_x;
  auto max_y = this->m_shapes[0]->bound_box().get_a().get_y();
  auto min_y = max_y;

  for (auto &shape : this->m_shapes) {
    auto b_b = shape->bound_box();

    /* majme auto a a auto b, nech auto a ide z mesta a do mesta b a auto b z
    mesta b do mesta a */
    auto a = b_b.get_a();
    auto b = b_b.get_b();
    min_x = min(min_x, min(a.get_x(), b.get_x()));
    min_y = min(min_y, min(a.get_y(), b.get_y()));
    max_x = max(max_x, max(a.get_x(), b.get_x()));
    max_y = max(max_y, max(a.get_y(), b.get_y()));
  }

  return Rect(Point(min_x, min_y), Point(max_x, max_y));
}

void Group::move(const double x_offset, const double y_offset) {
  for (auto &shape : this->m_shapes) {
    shape->move(x_offset, y_offset);
  }
}

void Group::add(unique_ptr<Shape> s) {
  this->m_shapes.push_back(std::move(s));
}

void Group::flatten() {
  while (!this->m_shapes.empty()) {
    Group *grp = dynamic_cast<Group *>(this->m_shapes.back().get());
    if (grp != nullptr) {
      grp->flatten();
    }
    this->m_shapes.pop_back();
  }
}

Group::Group(const Group &other) {
  for (auto &shp:other.m_shapes) {
    this->m_shapes.push_back(std::move(shp->clone()));
  }
}

unique_ptr<Shape> Group::clone() {
  return std::move(unique_ptr<Shape>(new Group(*this)));
}

Group Group::operator=(const Group &rhs) {
  if(&rhs == this){
    return *this;
  }

  this->m_shapes.clear();
  for (auto &shp:rhs.m_shapes) {
    this->m_shapes.push_back(std::move(shp->clone()));
  }

  return *this;
}
