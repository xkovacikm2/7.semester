//
// Created by kovko on 14.10.2016.
//

#ifndef ZADANIE_3_GROUP_H
#define ZADANIE_3_GROUP_H


#include <vector>
#include <memory>
#include "Shape.h"
#include "Rect.h"

/**
 * Container for Shapes implementing all of its methods.
 */
class Group : public Shape {
public:
  Group() {}

  Group(const Group &other);

  Group operator=(const Group &rhs);

  /**
   * Clones object because copy constructor is too mainstream.
   * @return
   */
  std::unique_ptr<Shape> clone() override;

  /**
   * @return bounding box encapsulating all objects inside container. if empty,
   * returns dummy Rect(Point(0,0), Point(0,0))
   */
  Rect bound_box() const override;

  /**
   * Moves recursively all objects in the container.
   * @param x_offset
   * @param y_offset
   */
  void move(const double x_offset, const double y_offset) override;

  /**
   * Adds shape to container.
   * @param s
   */
  void add(std::unique_ptr<Shape> s);

  /**
   * Recursively disbands all containers within container, then disbands itself.
   */
  void flatten();

  ~Group() {}

private:
  std::vector<std::unique_ptr<Shape>> m_shapes;
};


#endif //ZADANIE_3_GROUP_H
