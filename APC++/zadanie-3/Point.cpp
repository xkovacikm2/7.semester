//
// Created by kovko on 12.10.2016.
//
#include "Point.h"

Point::Point(double x, double y): m_x(x), m_y(y) {
}

double const Point::get_x() const {
  return this->m_x;
}

double const Point::get_y() const {
  return this->m_y;
}

