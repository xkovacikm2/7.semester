//
// Created by kovko on 12.10.2016.
//

#ifndef ZADANIE_3_POINT_H
#define ZADANIE_3_POINT_H


class Point {
public:
  /**
   * @param x
   * @param y
   * @return Point with assigned x and y coordinates.
   */
  Point(double x, double y);

  /**
   * Getter for x coordinate.
   * @return x
   */
  double const get_x() const;

  /**
   * Getter for y coordinate.
   * @return y
   */
  double const get_y() const;

  Point() = delete;
  ~Point() {}

private:
  double m_x;
  double m_y;
};


#endif //ZADANIE_3_POINT_H
