//
// Created by kovko on 13.10.2016.
//

#include "Rect.h"

using namespace std;

Rect::Rect(const Point &a, const Point &b): m_a(a), m_b(b) {
}

Rect Rect::bound_box() const {
  return Rect(*this);
}

void Rect::move(const double x_offset, const double y_offset) {
  this->m_a = Point(this->m_a.get_x() + x_offset, this->m_a.get_y() + y_offset);
  this->m_b = Point(this->m_b.get_x() + x_offset, this->m_b.get_y() + y_offset);
}

Point Rect::get_a() const {
  return this->m_a;
}

Point Rect::get_b() const {
  return this->m_b;
}

unique_ptr<Shape> Rect::clone() {
  return std::move(unique_ptr<Shape>(new Rect(*this)));
}
