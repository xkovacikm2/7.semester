//
// Created by kovko on 13.10.2016.
//

#ifndef ZADANIE_3_RECT_H
#define ZADANIE_3_RECT_H

#include "Shape.h"
#include "Point.h"

/**
 * Rect is defined by 2 points in opposite corners.
 */
class Rect : public Shape {
public:
  Rect(const Point &a, const Point &b);

  /**
   * Clones object because copy constructor is too mainstream.
   * @return
   */
  std::unique_ptr<Shape> clone() override;

  /**
   * @return clone of self.
   */
  Rect bound_box() const override;

  /**
   * Both points are moved by offset.
   * @param x_offset
   * @param y_offset
   */
  void move(const double x_offset, const double y_offset) override;

  /**
   * @return a.
   */
  Point get_a() const;

  /**
   * @return b.
   */
  Point get_b() const;

  ~Rect(){}
private:
  Point m_a;
  Point m_b;
};


#endif //ZADANIE_3_RECT_H
