//
// Created by kovko on 13.10.2016.
//

#ifndef ZADANIE_3_SHAPE_H
#define ZADANIE_3_SHAPE_H

#include <memory>

class Rect;

class Shape {
public:
  /**
   * @return Smallest rectangle that object fits in.
   */
  virtual Rect bound_box() const = 0;

  /**
   * Moves shape in plane by given offset.
   * @param x_offset
   * @param y_offset
   */
  virtual void move(const double x_offset, const double y_offset) = 0;

  /**
   * Clones object because copy constructor is too mainstream.
   * @return
   */
  virtual std::unique_ptr<Shape> clone() = 0;

  virtual ~Shape() {};
};


#endif //ZADANIE_3_SHAPE_H
