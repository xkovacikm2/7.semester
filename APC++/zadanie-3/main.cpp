
#include <iostream>
#include "Point.h"
#include "Rect.h"
#include "Circle.h"
#include "Group.h"

using namespace std;

int main() {
  for (int i = 0; i < 9999999; i++) {
    unique_ptr<Group> g1(new Group);
    unique_ptr<Shape> r1(new Rect(Point(1, 1), Point(2, 2)));
    unique_ptr<Group> g2(new Group);
    unique_ptr<Shape> g3(new Group);
    unique_ptr<Shape> c1(new Circle(Point(3, 3), 4));
    g2->add(move(c1));
    g2->add(move(g3));
    g1->add(move(g2));
    g1->add(move(r1));
    Group g4(*g1);
    Group g5;
    g5=g4;
    auto bb1 = g1->bound_box();
    auto bb3 = g4.bound_box();
    auto bb4 = g5.bound_box();
    g1->move(3, 3);
    auto bb2 = g1->bound_box();
    auto bb5 = g4.bound_box();
    auto bb6 = g5.bound_box();
    g1->flatten();
  }
  return 0;
}