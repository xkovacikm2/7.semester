//
// Created by kovko on 21.10.2016.
//

#include "BigNum.h"
#include <algorithm>
#include <regex>

using namespace std;

BigNum::BigNum() {
  this->initialize("0", false);
}

BigNum::BigNum(const string &str_number) {
  if (!regex_match(str_number, regex("^[-]?[1-9]{1}[0-9]*$"))) {
    this->initialize("0", false);
    return;
  }

  this->m_negative = (str_number[0] == '-');
  this->m_num = str_number;
  this->m_num.erase(remove(this->m_num.begin(), this->m_num.end(), '-'),
                    this->m_num.end()); //erase - if present
  reverse(this->m_num.begin(), this->m_num.end());
}

BigNum BigNum::operator-() const {
  BigNum ret(*this);
  if (ret.m_num != "0") {
    ret.m_negative = !this->m_negative;
  }
  return ret;
}

const BigNum BigNum::operator+() const {
  return *this;
}

BigNum &BigNum::operator+=(const BigNum &rhs) {
  if ((this->m_negative && rhs.m_negative)
      || (!this->m_negative && !rhs.m_negative)) {
    this->m_num = this->add(rhs);
  } else if (this->abs() < rhs.abs()) {
    this->initialize(rhs.sub(*this), !this->m_negative && rhs.m_negative);
  } else if (this->abs() > rhs.abs()) {
    this->initialize(this->sub(rhs), this->m_negative && !rhs.m_negative);
  } else {
    this->initialize("0", false);
  }
  this->slice_zeros();
  return *this;
}

BigNum &BigNum::operator-=(const BigNum &rhs) {
  if ((this->m_negative && !rhs.m_negative)
      || (!this->m_negative && rhs.m_negative)) {
    this->m_num = this->add(rhs);
  } else if (this->abs() > rhs.abs()) {
    this->initialize(this->sub(rhs), this->m_negative && rhs.m_negative);
  } else if (this->abs() < rhs.abs()) {
    this->initialize(rhs.sub(*this), !this->m_negative && !rhs.m_negative);
  } else {
    this->initialize("0", false);
  }
  this->slice_zeros();
  return *this;
}

BigNum &BigNum::operator*=(const BigNum &rhs) {
  BigNum this_copy(*this);
  this->initialize("0", !((this->m_negative && rhs.m_negative) ||
                          (!this->m_negative && !rhs.m_negative)));
  for (BigNum i; i < rhs.abs(); ++i) {
    this->m_num = this->add(this_copy);
  }
  return *this;
}

BigNum operator+(BigNum lhs, const BigNum &rhs) {
  lhs += rhs;
  return lhs;
}

BigNum operator-(BigNum lhs, const BigNum &rhs) {
  lhs -= rhs;
  return lhs;
}

BigNum operator*(BigNum lhs, const BigNum &rhs) {
  lhs *= rhs;
  return lhs;
}

bool BigNum::operator==(const BigNum &rhs) const {
  return (this->m_num == rhs.m_num && this->m_negative == rhs.m_negative);
}

bool BigNum::operator!=(const BigNum &rhs) const {
  return rel_ops::operator!=(*this, rhs);
}

bool BigNum::operator<(const BigNum &rhs) const {
  //true if this is negative and rhs is not
  if (this->m_negative && !rhs.m_negative) {
    return true;
  }
    //false if this is positive and rhs is negative
  else if (!this->m_negative && rhs.m_negative) {
    return false;
  }
    //if both are negative
  else if (this->m_negative && rhs.m_negative) {
    //if this is longer than rhs it is lower
    if (this->m_num.size() > rhs.m_num.size()) {
      return true;
    }
      //if this is shorter then it is greater
    else if (this->m_num.size() < rhs.m_num.size()) {
      return false;
    } else {
      return (BigNum::reverse_compare(this->m_num, rhs.m_num) > 0);
    }
  }
    //if both are positive
  else {
    //if this is longer it is greater
    if (this->m_num.size() > rhs.m_num.size()) {
      return false;
    }
      //if this is shorter, it is lower
    else if (this->m_num.size() < rhs.m_num.size()) {
      return true;
    }
  }
  return (BigNum::reverse_compare(this->m_num, rhs.m_num) < 0);
}

bool BigNum::operator>(const BigNum &rhs) const {
  return rel_ops::operator>(*this, rhs);
}

bool BigNum::operator<=(const BigNum &rhs) const {
  return rel_ops::operator<=(*this, rhs);
}

bool BigNum::operator>=(const BigNum &rhs) const {
  return rel_ops::operator>=(*this, rhs);
}

string BigNum::add(const BigNum &rhs) const {
  string result;
  int8_t overflow = 0;
  auto this_it = this->m_num.begin();
  auto rhs_it = rhs.m_num.begin();

  while (this_it != this->m_num.end() || rhs_it != rhs.m_num.end() ||
         overflow != 0) {

    //set value to overflow from previous cycle
    int8_t tmp_res = overflow;
    if (this_it != this->m_num.end()) {
      tmp_res += ((*this_it) - '0');
      this_it++;
    }
    if (rhs_it != rhs.m_num.end()) {
      tmp_res += ((*rhs_it) - '0');
      rhs_it++;
    }
    overflow = tmp_res / (static_cast<int8_t>(10));

    //crop result if necessary (modulo isn't free, therefore check if needed)
    if (overflow > 0) {
      tmp_res %= 10;
    }
    result.push_back(static_cast<char>(tmp_res + '0'));
  }
  return result;
}

string BigNum::sub(const BigNum &rhs) const {
  string result;
  int8_t overflow = 0;
  auto this_it = this->m_num.begin();
  auto rhs_it = rhs.m_num.begin();

  while (this_it != this->m_num.end() || rhs_it != rhs.m_num.end()) {
    //initialize difference to overflow
    int8_t to_sub = overflow;
    if (rhs_it != rhs.m_num.end()) {
      to_sub += ((*rhs_it) - '0');
      rhs_it++;
    }
    int8_t val = 0;
    if (this_it != this->m_num.end()) {
      val = ((*this_it) - '0');
      this_it++;
    }
    //set overflow if difference is greater than original value
    if (val < to_sub) {
      val += 10;
      overflow = 1;
    }

    val -= to_sub;
    result.push_back(static_cast<char>(val + '0'));
  }
  return result;
}

void BigNum::initialize(const string &num, const bool &negative) {
  this->m_num = num;
  this->m_negative = negative;
}

int BigNum::reverse_compare(std::string compared, std::string comparing) {
  reverse(compared.begin(), compared.end());
  reverse(comparing.begin(), comparing.end());
  return compared.compare(comparing);
}

BigNum BigNum::abs() const {
  BigNum ret(*this);
  ret.m_negative = false;
  return ret;
}

std::ostream &operator<<(std::ostream &lhs, const BigNum &rhs) {
  string val = rhs.m_num;
  if (rhs.m_negative) {
    val.push_back('-');
  }
  reverse(val.begin(), val.end());
  lhs << val;
  return lhs;
}

void BigNum::slice_zeros() {
  while (this->m_num.back() == '0' && this->m_num.size() > 1) {
    this->m_num.pop_back();
  }
}

BigNum BigNum::operator++() {
  *this += BigNum(1);
  return (*this);
}

BigNum &BigNum::operator/=(const BigNum &rhs) {
  if (rhs == BigNum(0)) {
    throw std::invalid_argument("division by zero");
  }

  BigNum this_copy(*this);
  for (this->initialize("0", false);
       this_copy.abs() >= rhs.abs();
       ++(*this)) {
    this_copy.m_num = this_copy.sub(rhs);
    this_copy.slice_zeros();
  }

  if (*this != BigNum(0)) {
    this->m_negative = !((this_copy.m_negative && rhs.m_negative) ||
                         (!this_copy.m_negative && !rhs.m_negative));
  }

  return *this;
}

BigNum operator/(BigNum lhs, const BigNum &rhs) {
  lhs /= rhs;
  return lhs;
}
