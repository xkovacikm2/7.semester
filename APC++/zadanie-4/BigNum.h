//
// Created by kovko on 21.10.2016.
//

#ifndef ZADANIE_4_BIGNUM_H
#define ZADANIE_4_BIGNUM_H

#include <cstdint>
#include <string>
#include <vector>

class BigNum {
public:
  BigNum();

  BigNum(int64_t n) : BigNum(std::to_string(n)) {}

  /**
   * If invalid string is provided, zero initializer is invoked.
   * @param str_number
   * @return
   */
  explicit BigNum(const std::string &str_number);

  BigNum operator-() const;

  BigNum abs() const;

  const BigNum operator+() const;

  BigNum &operator+=(const BigNum &rhs);

  BigNum operator++();

  BigNum &operator-=(const BigNum &rhs);

  BigNum &operator*=(const BigNum &rhs);

  BigNum &operator/=(const BigNum &rhs);

  bool operator==(const BigNum &rhs) const;

  bool operator!=(const BigNum &rhs) const;

  bool operator<(const BigNum &rhs) const;

  bool operator>(const BigNum &rhs) const;

  bool operator<=(const BigNum &rhs) const;

  bool operator>=(const BigNum &rhs) const;

  ~BigNum() {}

private:
  /**
   * Container for data.
   */
  std::string m_num;

  /**
   * Flag for negative values.
   */
  bool m_negative;

  /**
   * Adding. Muscle behind operators.
   * @param rhs
   * @return string with result.
   */
  std::string add(const BigNum &rhs) const;

  /**
   * Subtracting. Muscle behind operators.
   * @param rhs
   * @return string with result.
   */
  std::string sub(const BigNum &rhs) const;

  /**
   * Reverse comparison of strings.
   * @param compared
   * @param comparing
   * @return 0	They compare equal
   * <0	Either the value of the first character that does not match is
   *    lower in the compared string, or all compared characters match but
   *    the compared string is shorter.
   * >0	Either the value of the first character that does not match is
   *    greater in the compared string, or all compared characters match but
   *    the compared string is longer.
   */
  static int reverse_compare(std::string compared, std::string comparing);

  void initialize(const std::string &num, const bool &negative);

  /**
   * Removes trailing zeros.
   */
  void slice_zeros();

  friend BigNum operator+(BigNum lhs, const BigNum &rhs);

  friend std::ostream &operator<<(std::ostream &lhs, const BigNum &rhs);

  friend BigNum operator-(BigNum lhs, const BigNum &rhs);

  friend BigNum operator*(BigNum lhs, const BigNum &rhs);

  friend BigNum operator/(BigNum lhs, const BigNum &rhs);
};


#endif //ZADANIE_4_BIGNUM_H
