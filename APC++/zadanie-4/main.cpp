#include <iostream>
#include "BigNum.h"

using namespace std;

int main() {
  BigNum res(1);
  for(BigNum i(1); i <= BigNum(100); ++i){
    res *= i;
  }

  cout << res << endl;
  return 0;
}