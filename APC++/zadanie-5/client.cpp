#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>

#define BUFFER_SIZE 1024

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int client_socket_fd;
    uint16_t port_number;
    struct sockaddr_in server_address;
    struct hostent *server;

    char buffer[256];

    if (argc < 4) {
        std::cerr << "Error: Not enough arguments provided. IP, Port or Filename missing." << std::endl;
        exit(1);
    }



    port_number = std::stoul(argv[2]);
    client_socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (client_socket_fd < 0){
        std::cerr << "Error: Socket could not have been opened." << std::endl;
        exit(1);
    }

    server = gethostbyname(argv[1]);
    if (server == NULL) {
        std::cerr << "Error: Could not find host with specified address." << std::endl;
        exit(1);
    }

    server_address.sin_family = AF_INET;
    std::copy(server->h_addr, server->h_addr + server->h_length, (char *) &server_address.sin_addr.s_addr);
    server_address.sin_port = htons(port_number);

    if (connect(client_socket_fd,(struct sockaddr *) &server_address,sizeof(server_address)) < 0){
        std::cerr << "Error: Could not connect to server." << std::endl;
        exit(1);
    }
    struct stat file_stat;
    if(stat(argv[3], &file_stat) != 0){
        std::cerr << "Error: File error." << std::endl;
        exit(1);
    }
    off_t file_size = file_stat.st_size;
    std::string my_string(std::to_string((file_size % BUFFER_SIZE == 0)?(file_size/BUFFER_SIZE):(file_size/BUFFER_SIZE + 1)));
    ssize_t n = write(client_socket_fd,my_string.c_str(),my_string.size());
    if (n < 0){
        std::cerr << "Error: Something went wrong when writing to socket." << std::endl;
        exit(1);
    }
    std::ifstream input_file(argv[3]);
    if(!input_file.is_open()){
        std::cerr << "Error: Could not open file." << std::endl;
        exit(1);
    }
    while(!input_file.eof()){
        std::string line(BUFFER_SIZE, '\0');
        input_file.read(&line[0], BUFFER_SIZE);
        auto number = write(client_socket_fd, line.c_str(), line.size());
    }


    //bzero(buffer,256);
    std::string hash(BUFFER_SIZE, '\0');
    n = read(client_socket_fd,&hash[0],BUFFER_SIZE);
    if (n < 0){
        std::cerr << "Error: Something went wrong when reading from socket." << std::endl;
        exit(1);
    }
    //printf("%s\n",buffer);
    std::cout << hash << std::endl;




    close(client_socket_fd);
    return 0;
}
