/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[1024];
    struct sockaddr_in serv_addr, cli_addr;
    ssize_t n;

    if (argc < 2) {
        std::cerr << "Error: No port provided for server." << std::endl;
        exit(1);
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr,
             sizeof(serv_addr)) < 0)
        error("ERROR on binding");
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd,
                       (struct sockaddr *) &cli_addr,
                       &clilen);
    if (newsockfd < 0)
        error("ERROR on accept");
    bzero(buffer,1024);
    std::string number(1024, '\0');
    n = read(newsockfd,&number[0],1024);
    auto end = std::strtoul(number.c_str(), nullptr, 0);
    std::cout << end << "\n";
    if (n < 0) error("ERROR reading from socket");
    printf("Here is the message: %s\n",buffer);
    unsigned long long hash = 0;
    for(auto i = 0; i < end; ++i){
        std::string line(1024, '\0');
        //bzero(buffer,1024);
        n = read(newsockfd, &line[0], 1024);
        //buffer[1024] = '\0';
        std::cout << i << " " << line << "\n";
        //printf("Message %d: %s\n", i, buffer);
        for(auto member : line){
            hash += member;
        }
    }
    std::cout << hash << "\n";
    std::string return_hash = std::to_string(hash);
    n = write(newsockfd,return_hash.c_str(),return_hash.size());
    if (n < 0) error("ERROR writing to socket");
    close(newsockfd);
    close(sockfd);
    return 0;
}
