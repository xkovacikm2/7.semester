#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

vector<int> preprocess(string needle) {
  vector<int> pi(needle.size(), 0);
  int k = -1;
  pi[0] = k;
  for (size_t i = 1; i < needle.size(); i++) {

    //deescalate the pi until next character matches, or you reach beginning
    while (k > -1 && needle[k + 1] != needle[i]) {
      k = pi[k];
    }

    //if not in beginning, increment k
    if (needle[i] == needle[k + 1]) {
      k++;
    }

    pi[i] = k;
  }
  return pi;
}

string sanatize_white(char c) {
  if (c == '\n') return "\\n";
  if (c == '\t') return "\\t";
  return string(1, c);
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    cerr << "wrong number of arguments" << endl;
    return 1;
  }

  ifstream haystack(argv[1]);
  if (!haystack.is_open()) {
    cerr << "file cannot be opened" << endl;
    return 1;
  }

  string needle(argv[2]);

  if (needle.size() == 0){
    cerr << "cannot search for empty string" << endl;
    return 1;
  }

  auto pi = preprocess(needle);
  uint64_t position = 0;
  int k = -1;
  char head;
  while ((head = haystack.get()) != EOF) {
    position++;
    while (k > -1 && needle[k + 1] != head) {
      k = pi[k];
    }
    if (head == needle[k + 1]) {
      k++;
    }
    if (k + 1 == needle.size()) {
      //find prefix
      string prefix;
      if ((int64_t)(position - k - 3) < 0) {
        haystack.seekg(0);
        for(size_t i = 0; i < position - k; i++){
          prefix += sanatize_white(haystack.get());
        }
      } else {
        haystack.seekg(-((int) needle.size() + 3), ios_base::cur);
        for(size_t i = 0; i < 3; i++){
          prefix += sanatize_white(haystack.get());
        }
      }

      //find postfix
      string postfix;
      haystack.seekg(position);
      for(size_t i=0; i<3; i++){
        char tmp;
        if ((tmp = haystack.get()) == EOF){
          break;
        }
        postfix += sanatize_white(tmp);
      }

      haystack.seekg(position);
      cout << (position - k) << ": " << prefix << "..." << postfix << endl;
    }
  }

  return 0;
}
