//
// Created by kovko on 11.11.2016.
//

#include <cstring>
#include <iostream>
#include "SecureString.h"
#include "crypto.h"

using namespace std;

SecureString::SecureString(const char *c_arr)
  : m_str_size(strlen(c_arr)) {
  auto lel = code(ProtectMemory, c_arr);
  m_str.reset(lel);
}

SecureString::SecureString(const SecureString &other)
  : m_str_size(other.m_str_size) {
  unique_ptr<char> buf(new char[other.get_aligned_size()]);
  strncpy(buf.get(), other.m_str.get(), other.get_aligned_size());
  swap(this->m_str, buf);
}

SecureString::SecureString(SecureString &&other)
  : m_str_size(move(other.m_str_size)), m_str(move(other.m_str)) {
}

SecureString::~SecureString() {
}

SecureString SecureString::operator+=(const SecureString &rhs) {
  size_t new_size = this->m_str_size + rhs.m_str_size + 1;
  char res[new_size];

  this->safe_cstring(strcpy, res,
                     this->code(UnprotectMemory, this->m_str.get()));
  this->safe_cstring(strcat, res, rhs.code(UnprotectMemory, rhs.m_str.get()));
  *this = SecureString(res);
  return *this;
}

size_t SecureString::length() {
  return this->m_str_size;
}

void SecureString::clear() {
  *this = SecureString();
}

void SecureString::append_char(char c) {
  char c_str[2] = {c, '\0'};
  *this += SecureString(c_str);
}

void SecureString::remove_at(size_t at) {
  if (at >= this->m_str_size) return;
  unique_ptr<char> decoded(code(UnprotectMemory, this->m_str.get()));
  if (at == this->m_str_size - 1) {
    decoded.get()[at] = '\0';
    this->m_str_size = strlen(decoded.get());
    this->m_str.reset(code(ProtectMemory, decoded.get()));
  } else {
    unique_ptr<char> buf(new char[this->m_str_size]);
    decoded.get()[at] = '\0';
    strcpy(buf.get(), decoded.get());
    strcat(buf.get(), &(decoded.get()[at + 1]));
    this->m_str_size = strlen(buf.get());
    this->m_str.reset(code(ProtectMemory, buf.get()));
  }
}

void SecureString::set_at(size_t idx, char c) {
  if (idx >= this->m_str_size) return;
  shared_ptr<char> decoded(code(UnprotectMemory, this->m_str.get()));
  decoded.get()[idx] = c;
  this->m_str.reset(code(ProtectMemory, decoded.get()));
}

SecureStringUnprotected SecureString::get_unprotected() const {
  return SecureStringUnprotected(code(UnprotectMemory, this->m_str.get()));
}

char *SecureString::code(bool func(void *, size_t), const char *c) const {
  size_t size = this->get_aligned_size();

  char *buf = new char[size];
  strncpy(buf, c, size);
  func(buf, size);

  return buf;
}

size_t SecureString::get_aligned_size() const {
  size_t size = this->m_str_size + 1;
  if (size % SECURE_MEMORY_BLOCK != 0) {
    size += SECURE_MEMORY_BLOCK - (size % SECURE_MEMORY_BLOCK);
  }
  return size;
}

void SecureString::safe_cstring(char *cstr_func(char *, const char *),
                                char *destination, char *source) {
  cstr_func(destination, source);
  CleanMemory(source, strlen(source));
  delete[] source;
}

SecureString SecureString::operator=(const SecureString &rhs) {
  this->m_str_size = rhs.m_str_size;
  this->m_str.reset(new char[rhs.get_aligned_size()]);
  strncpy(this->m_str.get(), rhs.m_str.get(), rhs.get_aligned_size());
  return *this;
}

SecureString SecureString::operator=(SecureString &&rhs) {
  this->m_str_size = move(rhs.m_str_size);
  swap(this->m_str, rhs.m_str);
  return *this;
}

