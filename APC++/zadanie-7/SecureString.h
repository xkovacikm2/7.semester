//
// Created by kovko on 11.11.2016.
//

#ifndef ZADANIE_7_SECURESTRING_H
#define ZADANIE_7_SECURESTRING_H


#include <cstddef>
#include <memory>
#include "SecureStringUnprotected.h"

class SecureString {
public:
  SecureString(const char *c_arr);
  SecureString():SecureString(""){};
  SecureString(const SecureString &other);
  SecureString(SecureString &&other);
  ~SecureString();

  //operators
  SecureString operator+=(const SecureString &rhs);
  SecureString operator=(const SecureString &rhs);
  SecureString operator=(SecureString &&rhs);

  //interface
  size_t length();
  void clear();
  void append_char(char c);
  void remove_at(size_t at);
  void set_at(size_t idx, char c);
  SecureStringUnprotected get_unprotected() const;

private:
  char *code(bool func(void*, size_t), const char *c) const;
  size_t get_aligned_size() const;
  void safe_cstring(char *cstr_func(char*, const char *), char* destination, char* source);
  //attributes
  size_t m_str_size;
  std::unique_ptr<char> m_str;
};


#endif //ZADANIE_7_SECURESTRING_H
