//
// Created by kovko on 11.11.2016.
//

#include <cstring>
#include "SecureStringUnprotected.h"
#include "crypto.h"

SecureStringUnprotected::SecureStringUnprotected(char *c_string)
  : m_str(c_string) {
}

char *SecureStringUnprotected::c_str() const {
  return this->m_str.get();
}

SecureStringUnprotected::~SecureStringUnprotected() {
  if (this->m_str.get() != nullptr)
    CleanMemory(this->m_str.get(), strlen(this->m_str.get()));
}
