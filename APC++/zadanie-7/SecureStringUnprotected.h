//
// Created by kovko on 11.11.2016.
//

#ifndef ZADANIE_7_SECURESTRINGUNPROTECTED_H
#define ZADANIE_7_SECURESTRINGUNPROTECTED_H


#include <memory>

class SecureStringUnprotected {
public:
  SecureStringUnprotected(char* c_string);
  SecureStringUnprotected(const SecureStringUnprotected &other) = delete;
  SecureStringUnprotected(SecureStringUnprotected &&other) = default;
  ~SecureStringUnprotected();

  char* c_str() const;
private:
  std::shared_ptr<char> m_str;
};


#endif //ZADANIE_7_SECURESTRINGUNPROTECTED_H
