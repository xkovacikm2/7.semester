//
// Created by kovko on 11.11.2016.
//

#ifndef ZADANIE_7_CRYPTO_H
#define ZADANIE_7_CRYPTO_H
#ifdef WIN32
#include <Windows.h>

#define SECURE_MEMORY_BLOCK CRYPTPROTECTMEMORY_BLOCK_SIZE
#else
#define SECURE_MEMORY_BLOCK 8
#endif
#include <cstddef>

bool ProtectMemory(void *data, size_t len);
bool UnprotectMemory(void *data, size_t len);
void CleanMemory(void *data, size_t len);

#endif //ZADANIE_7_CRYPTO_H
