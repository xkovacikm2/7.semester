#include <iostream>
#include "SecureString.h"

using namespace std;

int main() {
  for (uint64_t i = 0; i < 10000000; i++) {
    SecureString s("magori ");
    SecureString a("su vsade");
    s += a;

    s.append_char('!');

    s.remove_at(99);
    s.remove_at(15);
    s.remove_at(4);

    s.set_at(4, 'c');
    s.set_at(60, 'x');

    auto o = s.get_unprotected();
    SecureStringUnprotected q(move(o));

    cout << string(q.c_str()) << endl;
  }
  return 0;
}
