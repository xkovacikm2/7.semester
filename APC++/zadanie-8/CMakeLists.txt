cmake_minimum_required(VERSION 3.6)
project(zadanie_8)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

set(SOURCE_FILES main.cpp JSON.cpp JSON.h)
add_executable(zadanie_8 ${SOURCE_FILES})