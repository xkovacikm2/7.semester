// Created by kovko on 16.11.2016.
//

#include <sstream>
#include "JSON.h"

using namespace std;

JSON::JSON(const std::string &json) {
  this->parse(json);
}

int64_t JSON::get_int(const std::string &key) {
  return m_i_map.at(key);
}

std::string JSON::get_str(const std::string &key) {
  return m_s_map.at(key);
}

void JSON::set(const std::string &key, int64_t value) {
  this->m_s_map.erase(key);
  this->m_i_map[key] = value;
}

void JSON::set(const std::string &key, const std::string& value) {
  this->m_i_map.erase(key);
  this->m_s_map[key] = value;
}

string JSON::encode_json() {
  ostringstream builder;
  builder << "{";

  string separator("\n\t");
  for (const auto &pair : this->m_s_map) {
    builder << separator << '"' << pair.first << "\": \"" << pair.second << "\"";
    if (separator[0] != ',') {
      separator = ",\n\t";
    }
  }

  for (const auto &pair : this->m_i_map) {
    builder << separator << '"' << pair.first << "\": " << pair.second << "";
    if (separator[0] != ',') {
      separator = ",\n\t";
    }
  }

  builder << "\n}";
  return builder.str();
}

void JSON::parse(const std::string &json) {
  istringstream in(json);
  skip_white_chars(in);

  if (in.peek() != '{')
    throw invalid_argument("unexpected character, expecting '{'");

  //only '}'. ',' and '{' are expected and can be discarded by get
  while (in.get() != '}') {
    skip_white_chars(in);
    auto key = get_s_value(in);

    skip_white_chars(in);
    if (in.get() != ':')
      throw invalid_argument("unexpected character, expecting ':'");
    skip_white_chars(in);

    if (isdigit(in.peek()) || in.peek() == '-') {
      in >> this->m_i_map[key];
    } else {
      this->m_s_map[key] = get_s_value(in);
    }

    skip_white_chars(in);
    if (in.peek() != ',' && in.peek() != '}')
      throw invalid_argument("unexpected character, expecting ',' or '}'");
  }
}

void JSON::skip_white_chars(std::istringstream &stream) {
  while (isspace(stream.peek())) {
    stream.get();
  }
}

std::string JSON::get_s_value(std::istringstream &stream) {
  if (stream.get() != '"')
    throw invalid_argument("unexpected character, expecting '\"'");
  ostringstream builder;
  char c;
  while ((c = stream.get()) != '"') {
    if(stream.eof()){
      throw std::invalid_argument("unexpected end of JSON");
    }
    builder << c;
  }
  return builder.str();
}

JSON_TYPE JSON::get_type(const std::string &key) {
  if(this->m_i_map.find(key) != this->m_i_map.end()){
    return JSON_TYPE::INTEGER;
  }
  if(this->m_s_map.find(key) != this->m_s_map.end()){
    return JSON_TYPE::STR1NG;
  }
  return JSON_TYPE::NONE;
}

