//
// Created by kovko on 16.11.2016.
//

#ifndef ZADANIE_8_JSON_H
#define ZADANIE_8_JSON_H

#include <string>
#include <unordered_map>

enum class JSON_TYPE {
  INTEGER, STR1NG, NONE
};

class JSON {
public:
  JSON(const std::string &json);
  JSON(const JSON &other) = default;
  JSON(JSON &&other) = default;
  JSON &operator=(const JSON &rhs) = default;
  JSON &operator=(JSON &&rhs) = default;
  ~JSON() {};

  //interface
  JSON_TYPE get_type(const std::string &key);
  int64_t get_int(const std::string &key);
  std::string get_str(const std::string &key);
  void set(const std::string &key, int64_t value);
  void set(const std::string &key, const std::string &value);
  std::string encode_json();

private:
  void parse(const std::string &json);
  void skip_white_chars(std::istringstream &stream);
  std::string get_s_value(std::istringstream &stream);
  std::unordered_map<std::string, int64_t> m_i_map;
  std::unordered_map<std::string, std::string> m_s_map;
};


#endif //ZADANIE_8_JSON_H
