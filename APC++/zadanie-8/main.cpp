#include <iostream>
#include <sstream>
#include "JSON.h"

using namespace std;

int main() {
  JSON j("{\n\t\"name1\": \"value1\",\n\t\"name2\": \"value 2 (pozor v tomto stringu je medzera)\",\n\t\"name3 (aj tu je medzera)\": 1254\n}");
  auto rv = j.get_type("name2");
  rv = j.get_type("name3 (aj tu je medzera)");
  j.set("name2", -2);
  cout << j.encode_json() << endl;
  JSON k("{\"");
  return 0;
}