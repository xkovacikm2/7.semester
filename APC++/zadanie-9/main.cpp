#include <future>
#include <iostream>

int f(){
  int i;
  for(i = 0; i<100000; i++){
  }
  return i;
}

int main() {
  auto t1 = std::async(std::launch::deferred, f); //nevytvori novy thread, ale pusti to na tomto threade
  auto t2 = std::async(f);

  std::cout << t1.get() + t2.get();
  return 0;
}