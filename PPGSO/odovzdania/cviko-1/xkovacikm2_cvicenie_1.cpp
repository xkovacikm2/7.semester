#include <iostream>
#include <fstream>

// Size of the framebuffer
const unsigned int SIZE = 512;

// A simple RGB struct will represent a pixel in the framebuffer
// Note: unsigned char is range <0, 255>
//       signed char is range <-128, 127>
struct Pixel {
    unsigned char r, g, b;
};

int main() {
    // Initialize a framebuffer
    // NOTE: The framebuffer is allocated on the stack. For bigger sizes this should be changed to a heap allocation.
    Pixel framebuffer[SIZE][SIZE];

    // Task1: Load RAW image file here instead
    std::ifstream lena("lena.rgb", std::ios::binary);
    lena.read(reinterpret_cast<char *>(framebuffer), sizeof(framebuffer));
    //inverts colours in loaded picture
    for (unsigned int x = 0; x < SIZE; ++x) {
        for (unsigned int y = 0; y < SIZE; ++y) {
            framebuffer[x][y].r = (unsigned char) (0xff - framebuffer[x][y].r);
            framebuffer[x][y].g = (unsigned char) (0xff - framebuffer[x][y].g);
            framebuffer[x][y].b = (unsigned char) (0xff - framebuffer[x][y].b);
        }
    }

    // Save the raw image to a file
    std::cout << "Generating result.rgb file ..." << std::endl;
    std::ofstream raw("result.rgb", std::ios::binary);
    raw.write(reinterpret_cast<char *>(framebuffer), sizeof(framebuffer));
    raw.close();

    std::cout << "Done." << std::endl;
    return EXIT_SUCCESS;
}
