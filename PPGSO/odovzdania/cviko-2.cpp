// Example raw_gradient
// - Illustrates the concept of a framebuffer
// - We do not really need any libraries or hardware to do computer graphics
// - In this case the framebuffer is simply saved as a raw RGB and TGA image

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

// Size of the framebuffer
const unsigned int SIZE = 512;

// A simple RGB struct will represent a pixel in the framebuffer
// Note: unsigned char is range <0, 255>
//       signed char is range <-128, 127>
struct Pixel {
  unsigned char r, g, b;
};

struct coords {
  int x;
  int y;
};

void drawLine(coords c1, coords c2, Pixel framebuffer[][SIZE]) {
  float x1 = c1.x, x2 = c2.x, y1 = c1.y, y2 = c2.y;

  bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
  if (steep) {
    std::swap(x1, y1);
    std::swap(x2, y2);
  }

  if (x1 > x2) {
    std::swap(x1, x2);
    std::swap(y1, y2);
  }

  float dx = x2 - x1;
  float dy = (float) fabs(y2 - y1);

  float error = dx / 2.0f;
  int ystep = (y1 < y2) ? 1 : -1;
  int y = (int) y1;

  int maxX = (int) x2;

  for (int x = (int) x1; x < maxX; x++) {
    if (steep) {
      framebuffer[y+256][x+256].r = 0xff;
      framebuffer[y+256][x+256].g = 0xff;
      framebuffer[y+256][x+256].b = 0xff;
    } else {
      framebuffer[x+256][y+256].r = 0xff;
      framebuffer[x+256][y+256].g = 0xff;
      framebuffer[x+256][y+256].b = 0xff;
    }

    error -= dy;
    if (error < 0) {
      y += ystep;
      error += dx;
    }
  }
}

std::vector<coords> circlePoints(int n) {
  int angle = 360 / n;
  const int r = 200;
  std::vector<coords> c;
  for (int i = 0; i < 360; i += angle) {
    coords cor;
    cor.x = (int) (r * sin(i * M_PI / 180));
    cor.y = (int) (r * cos(i * M_PI / 180));
    c.push_back(cor);
  }
  return c;
}

int main() {
  // Initialize a framebuffer
  // NOTE: The framebuffer is allocated on the stack. For bigger sizes this should be changed to a heap allocation.
  Pixel framebuffer[SIZE][SIZE];

  //draw a star
  std::vector<coords> c = circlePoints(5);

  for (auto i:c) {
    for (auto j:c) {
      drawLine(i, j, framebuffer);
    }
  }

  // Save the raw image to a file
  std::cout << "Generating result.rgb file ..." << std::endl;
  std::ofstream raw("result.rgb", std::ios::binary);
  raw.write(reinterpret_cast<char *>(framebuffer), sizeof(framebuffer));
  raw.close();

  std::cout << "Done." << std::endl;
  return EXIT_SUCCESS;
}
