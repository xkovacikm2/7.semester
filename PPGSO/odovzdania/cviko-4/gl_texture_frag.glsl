#version 150
// A texture is expected as program attribute
uniform sampler2D Texture;

// The vertex shader fill feed this input
in vec2 FragTexCoord;

// The final color
out vec4 FragmentColor;

vec4 get_pixel(in vec2 coords, in float dx, in float dy) {
   return texture2D(Texture,coords + vec2(dx, dy));
}

float[25] get_makro_pixel(in int channel) {
   float dx = 1.0 / float(textureSize(Texture,0));
   float dy = 1.0 / float(textureSize(Texture,0));
   float[25] mat;
   int k = 0;
   for (int i=-2; i<3; i++) {
      for(int j=-2; j<3; j++) {
         mat[k] = get_pixel(FragTexCoord.xy,float(i)*dx,
                            float(j)*dy)[channel];
         k++;
      }
   }
   return mat;
}

float convolve(in float[25] filt, in float[25] matrix) {
   float res = 0.0;
   for (int i=0; i<25; i++) {
      res += filt[i]*matrix[i];
   }
   return clamp(res,0.0,1.0);
}

void main() {
  float[25] sharpness = float[] (-1.,-1.,-1., -1., -1.,
                                    -1.,-1.,-1., -1., -1.,
                                    -1., -1, 25., -1., -1.,
                                    -1.,-1.,-1., -1., -1.,
                                    -1., -1., -1., -1., -1.);

  float[25] makro_pixel_r = get_makro_pixel(0);
  float[25] makro_pixel_g = get_makro_pixel(1);
  float[25] makro_pixel_b = get_makro_pixel(2);

  FragmentColor = vec4(convolve(sharpness, makro_pixel_r),
                        convolve(sharpness, makro_pixel_g),
                        convolve(sharpness, makro_pixel_b), 1.0);
}
