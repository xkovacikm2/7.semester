//
// Created by kovko on 29.11.2016.
//

#include "MapGenerator.h"
MapGenerator::MapGenerator() {
  this->generateMap();
}

std::vector<int> MapGenerator::requestSpawnPoint(int ID) {
  int count = this->countPOI(ID);
  int rand = this->Rand(0,count) + 1;
  auto coords = this->findCoords(ID, rand);
  this->map[coords[1]][coords[0]] = 0;
  return coords;
}

float MapGenerator::Rand(float min, float max) {
  return ((max - min) * ((float) rand() / (float) RAND_MAX)) + min;
}

void MapGenerator::generateMap() {
  this->prepareMap();
  //this->print("This is just the begining!");
  this->finalizeMap();
  //this->print();
}

void MapGenerator::prepareMap() {
  for (int i = 0; i < this->size; i++) {
    for (int j = 0; j < this->size; j++) {
      //surrounding map
      if (i == 0 || i == (this->size - 1) || j == 0 || j == (this->size - 1)) {
        this->map[i][j] = 1;
      }
        //wall precursors
      else if ((i % 6 == 0) && (j % 6 == 0)) {
        this->map[i][j] = 2;
      }
        //colonel 1 spawnpoints
      else if ((j == 1) && (i % 3 == 1)) {
        this->map[i][j] = 3;
      }
        //colonel 2 spawnpoints
      else if ((j == this->size - 3) && (i % 3 == 1)) {
        this->map[i][j] = 4;
      }
        //spawn of exit point
      else if (((i == this->size - 3) || (i == this->size - 2)) &&
               ((j == this->size / 2 - 1) || (j == this->size / 2))) {
        this->map[i][j] = 'C' - '0';
      }
        //spawn of empty space
      else
        this->map[i][j] = 0;
    }
  }
}

void MapGenerator::finalizeMap() {
  int poi = 2;
  int pocet = this->countPOI(poi);
  //if any spawn points remain
  while (pocet > 0) {
    //I randomly pick spawnpoint
    int ktore = this->Rand(0, pocet) + 1;
    //find coordinates of randomly picked spawn point
    std::vector<int> suradnice = this->findCoords(poi, ktore);
    //randomly pick direction
    int smer = this->Rand(0, 4);
    //set vectors
    int dx, dy;
    dy = dx = 0;
    switch (smer) {
      //up
      case 0:
        dy = -1;
        break;
        //right
      case 1:
        dx = 1;
        break;
        //down
      case 2:
        dy = 1;
        break;
        //left
      case 3:
        dx = -1;
        break;
    }
    //create wall in given direction
    this->makeWall(dx, dy, suradnice);
    pocet = this->countPOI(poi);
  }
}

int MapGenerator::countPOI(int poi) {
  int count = 0;
  for (int i = 0; i < this->size; i++) {
    for (int j = 0; j < this->size; j++) {
      if (this->map[i][j] == poi) {
        count++;
      }
    }
  }
  return count;
}

std::vector<int> MapGenerator::findCoords(int value, int count) {
  std::vector<int> coords = {0, 0};
  int pocitadlo = 0;
  for (int y = 0; y < this->size; y++) {
    for (int x = 0; x < this->size; x++) {
      if (this->map[y][x] == value) {
        pocitadlo++;
      }
      if (pocitadlo == count) {
        coords[0] = x;
        coords[1] = y;
        return coords;
      }
    }
  }
  return coords;
}

void MapGenerator::makeWall(int dx, int dy, std::vector<int> coords) {
  int x, y;
  x = coords[0];
  y = coords[1];
  while (this->map[y][x] != 1) {
    this->map[y][x] = 1;
    y += dy;
    x += dx;
  }
}

