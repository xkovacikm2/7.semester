//
// Created by kovko on 29.11.2016.
//

#ifndef PPGSO_MAPGENERATOR_H
#define PPGSO_MAPGENERATOR_H

#include <glm/vec3.hpp>
#include <vector>

class MapGenerator {
public:
  int map[54][54];
  int size = 54;

  MapGenerator();
  std::vector<int> requestSpawnPoint(int ID);
  float Rand(float min, float max);
  std::vector<int> findCoords(int value, int count);

private:
  void generateMap();
  void prepareMap();
  void finalizeMap();
  int countPOI(int poi);
  void makeWall(int dx, int dy, std::vector<int> coords);
};


#endif //PPGSO_MAPGENERATOR_H
