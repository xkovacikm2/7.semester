//
// Created by kovko on 15.11.2016.
//

#include "camera.h"
#include <glm/gtc/type_ptr.hpp>
#include <math.h>

Camera::Camera(glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch)
  : Front(glm::vec3(0.0f, 1.0f, -1.0f)), MovementSpeed(Camera::SPEED),
    MouseSensitivity(Camera::SENSITIVITY), Zoom(Camera::ZOOM) {
  this->Position = position;
  this->projectionMatrix = glm::perspective(glm::radians(45.0f),
                                            GLfloat(800 / 800), 0.1f, 50.0f);
  this->WorldUp = up;
  this->Yaw = yaw;
  this->Pitch = pitch;
  this->updateCameraVectors();
}

glm::mat4 Camera::GetViewMatrix() {
  return glm::lookAt(this->Position, this->Position + this->Front, this->Up);
}

void Camera::ProcessKeyboard(Scene &scene, Camera_Movement direction,
                             GLfloat deltaTime) {
  auto last_position = this->Position;
  GLfloat velocity = this->MovementSpeed * deltaTime;
  if (direction == Camera_Movement::FORWARD)
    this->Position += this->Front * velocity;
  if (direction == Camera_Movement::BACKWARD)
    this->Position -= this->Front * velocity;
  if (direction == Camera_Movement::LEFT)
    this->Position -= this->Right * velocity;
  if (direction == Camera_Movement::RIGHT)
    this->Position += this->Right * velocity;
  this->Position.y = 1.0;

  //collision detection
  if (scene.map.map[(int) this->Position.x][(int) this->Position.z] == 1
      || scene.map.map[(int) this->Position.x + 1][(int) this->Position.z + 1] == 1) {
    this->Position = last_position;
  }
}

void Camera::ProcessMouseMovement(GLfloat x, GLfloat y) {
  GLfloat x_offset = x - lastX;
  GLfloat y_offset = y - lastY;
  lastX = x;
  lastY = y;
  x_offset *= this->MouseSensitivity;
  y_offset *= this->MouseSensitivity;

  this->Yaw += x_offset;
  this->Pitch -= y_offset;

  // Make sure that when pitch is out of bounds, screen doesn't get flipped
  if (this->Pitch > 89.0f)
    this->Pitch = 89.0f;
  if (this->Pitch < -89.0f)
    this->Pitch = -89.0f;

  // Update Front, Right and Up Vectors using the updated Eular angles
  this->updateCameraVectors();
}

void Camera::ProcessMouseScroll(GLfloat yoffset) {
  if (this->Zoom >= 1.0f && this->Zoom <= 45.0f)
    this->Zoom -= yoffset;
  if (this->Zoom <= 1.0f)
    this->Zoom = 1.0f;
  if (this->Zoom >= 45.0f)
    this->Zoom = 45.0f;
}

void Camera::updateCameraVectors() {
  // Calculate the new Front vector
  glm::vec3 front;
  front.x = cos(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
  front.y = sin(glm::radians(this->Pitch));
  front.z = sin(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
  this->Front = glm::normalize(front);
  // Also re-calculate the Right and Up vector
  this->Right = glm::normalize(glm::cross(this->Front,
                                          this->WorldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
  this->Up = glm::normalize(glm::cross(this->Right, this->Front));
}

