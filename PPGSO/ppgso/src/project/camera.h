//
// Created by kovko on 15.11.2016.
//

#ifndef PPGSO_CAMERA_H_H
#define PPGSO_CAMERA_H_H


// Std. Includes
#include <vector>

// GL Includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "scene.h"

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum class Camera_Movement {
  FORWARD,
  BACKWARD,
  LEFT,
  RIGHT
};

class Scene;
// An abstract camera class that processes input and calculates the corresponding Eular Angles, Vectors and Matrices for use in OpenGL
class Camera {
public:
  // Default camera values
  constexpr static const GLfloat YAW = 1.0f;
  constexpr static const GLfloat PITCH = 1.0f;
  constexpr static const GLfloat SPEED = 3.0f;
  constexpr static const GLfloat SENSITIVITY = 0.2f;
  constexpr static const GLfloat ZOOM = 1.0f;
  // perspective projection
  glm::mat4 projectionMatrix;
  GLfloat lastX = 800/2;
  GLfloat lastY = 800/2;

  // Camera Attributes
  glm::vec3 Position;
  glm::vec3 Front;
  glm::vec3 Up;
  glm::vec3 Right;
  glm::vec3 WorldUp;
  // Eular Angles
  GLfloat Yaw;
  GLfloat Pitch;
  // Camera options
  GLfloat MovementSpeed;
  GLfloat MouseSensitivity;
  GLfloat Zoom;

  // Constructor with vectors
  Camera(glm::vec3 position = glm::vec3(1.5f, 0.5f, 1.5f),
         glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW,
         GLfloat pitch = PITCH);

  // Returns the view matrix calculated using Eular Angles and the LookAt Matrix
  glm::mat4 GetViewMatrix();

  // Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
  void ProcessKeyboard(Scene &scene, Camera_Movement direction, GLfloat deltaTime);

  // Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
  void ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset);

  // Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
  void ProcessMouseScroll(GLfloat yoffset);

private:
  void updateCameraVectors();
};

#endif //PPGSO_CAMERA_H_H
