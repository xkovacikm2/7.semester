//
// Created by kovko on 16.11.2016.
//

#include "cube_frag.h"
#include "cube_vert.h"
#include "cube.h"

using namespace std;

Cube::Cube(GLfloat x, GLfloat z) {
  position = glm::vec3(x, 1.0, z);
  scale = glm::vec3(1.0, 2.0, 1.0);
  if (!shader) shader = shared_ptr<Shader>(new Shader{cube_vert, cube_frag});
  if (!texture) texture = shared_ptr<Texture>(new Texture{"wall.rgb", 100, 100});
  if (!mesh) mesh = shared_ptr<Mesh>(new Mesh{shader, "cube.obj"});
}

Cube::~Cube() {
}

bool Cube::Update(Scene &scene, float dt) {
  this->GenerateModelMatrix();
  return true;
}

void Cube::Render(glm::vec3 &camera_position,glm::mat4 &viewMatrix, glm::mat4 &projectMatrix) {
  shader->Use();

  // use camera
  shader->SetMatrix(projectMatrix, "ProjectionMatrix");
  shader->SetMatrix(viewMatrix, "ViewMatrix");
  GLint lightPositionLoc = shader->GetUniformLocation("lightPosition");
  glUniform3f(lightPositionLoc, camera_position.x, camera_position.y, camera_position.z);
  // render mesh
  shader->SetMatrix(modelMatrix, "ModelMatrix");
  shader->SetTexture(texture, "Texture");
  mesh->Render();
}

// shared resources
shared_ptr<Mesh> Cube::mesh;
shared_ptr<Shader> Cube::shader;
shared_ptr<Texture> Cube::texture;