//
// Created by kovko on 16.11.2016.
//

#ifndef PPGSO_CUBE_H
#define PPGSO_CUBE_H


#include <shader.h>
#include <mesh.h>
#include "object.h"

class Cube : public Object {
public:
  Cube(GLfloat x, GLfloat z);

  ~Cube();

  bool Update(Scene &scene, float dt);

  void Render(glm::vec3 &camera_position,glm::mat4 &viewMatrix, glm::mat4 &projectMatrix);

private:
  static std::shared_ptr<Shader> shader;
  static std::shared_ptr<Mesh> mesh;
  static std::shared_ptr<Texture> texture;
};


#endif //PPGSO_CUBE_H
