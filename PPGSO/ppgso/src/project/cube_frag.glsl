#version 330
// A texture is expected as program attribute
uniform sampler2D Texture;

// The vertex shader fill feed this input
in vec2 FragTexCoord;

// Wordspace normal
in vec4 normal;

in vec3 FragPos;

// The final color
out vec4 FragmentColor;

// Light from camera
uniform vec3 lightPosition;

void main() {
  vec3 ndLightPosition = vec3(1.5f, 0.5f, 1.5f);
  float ambient = 0.1;
  float ndAmbient = 0.2;
  // Compute diffuse lighting
  float diffuse = max(dot(vec3(normal), normalize(lightPosition-FragPos)), 0.0f);
  float ndDiffuse = max(dot(vec3(normal), normalize(ndLightPosition-FragPos)), 0.0f);

  float specularStrength = 0.7f;
  vec3 viewDir = normalize(lightPosition - FragPos);
  vec3 ndViewDir = normalize(ndLightPosition - FragPos);
  vec3 reflectDir = reflect(-normalize(lightPosition-FragPos), vec3(normal));
  vec3 ndReflectDir = reflect(-normalize(ndLightPosition-FragPos), vec3(normal));
  float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
  float ndSpec = pow(max(dot(ndViewDir, ndReflectDir), 0.0), 32);
  vec3 specular = (specularStrength * spec * vec3(1.0,1.0,1.0));
  vec3 ndSpecular = (specularStrength * ndSpec * vec3(1.0, 0.0, 0.0));
  // Lookup the color in Texture on coordinates given by fragTexCoord and apply diffuse lighting
  FragmentColor = texture(Texture, FragTexCoord) * vec4(ambient + diffuse + specular, 1.0);
  FragmentColor += texture(Texture, FragTexCoord) *  vec4(ambient + ndDiffuse + ndSpecular, 1.0f) ;
}
