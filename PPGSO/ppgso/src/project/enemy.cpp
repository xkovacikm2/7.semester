//
// Created by kovko on 2.12.2016.
//

#include <GL/glew.h>
#include "cube_vert.h"
#include "cube_frag.h"
#include "enemy.h"
#include "scene.h"

using namespace std;
Enemy::Enemy(GLfloat x, GLfloat z) {
  scale = glm::vec3(1.0, 1.0, 1.0);
  alive = true;
  if (!shader) shader = shared_ptr<Shader>(new Shader{cube_vert, cube_frag});
  if (!texture) texture = shared_ptr<Texture>(new Texture{"wall.rgb", 100, 100});
  if (!mesh) mesh = shared_ptr<Mesh>(new Mesh{shader, "ACM_Xenomorph.obj"});
}

bool Enemy::Update(Scene &scene, float dt) {
  this->GenerateModelMatrix();
  if((int)scene.camera->Position.x == (int)this->position.x && (int)scene.camera->Position.z == (int)this->position.z){
    scene.message = "You are dead";
    scene.game_over = true;
  }
  return alive;
}

void Enemy::Render(glm::vec3 &camera_position, glm::mat4 &viewMatrix,
                   glm::mat4 &projectMatrix) {
  shader->Use();

  // use camera
  shader->SetMatrix(projectMatrix, "ProjectionMatrix");
  shader->SetMatrix(viewMatrix, "ViewMatrix");
  GLint lightPositionLoc = shader->GetUniformLocation("lightPosition");
  glUniform3f(lightPositionLoc, camera_position.x, camera_position.y, camera_position.z);
  // render mesh
  shader->SetMatrix(modelMatrix, "ModelMatrix");
  shader->SetTexture(texture, "Texture");
  mesh->Render();
}

bool Enemy::DetectCollision(Object &other) {
  return ((int) this->position.x == (int) other.position.x
          && ((int) this->position.y == (int) other.position.y
              || (int) this->position.y +1 == (int) other.position.y)
          && (int) this->position.z == (int) other.position.z);
}
// shared resources
shared_ptr<Mesh> Enemy::mesh;
shared_ptr<Shader> Enemy::shader;
shared_ptr<Texture> Enemy::texture;