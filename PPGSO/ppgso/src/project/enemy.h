//
// Created by kovko on 2.12.2016.
//

#ifndef PPGSO_ENEMY_H
#define PPGSO_ENEMY_H

#include <GL/gl.h>
#include <shader.h>
#include <mesh.h>
#include "object.h"

class Enemy : public Object{
public:
  Enemy(GLfloat x, GLfloat z);

  ~Enemy() = default;

  bool Update(Scene &scene, float dt);

  void Render(glm::vec3 &camera_position,glm::mat4 &viewMatrix, glm::mat4 &projectMatrix);
  bool DetectCollision(Object &other);
private:
  static std::shared_ptr<Shader> shader;
  static std::shared_ptr<Mesh> mesh;
  static std::shared_ptr<Texture> texture;
};


#endif //PPGSO_ENEMY_H
