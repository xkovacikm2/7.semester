#ifndef PPGSO_EXPLOSION_H
#define PPGSO_EXPLOSION_H

#include "texture.h"
#include "shader.h"
#include "mesh.h"
#include "object.h"

class Explosion : public Object {
public:
  Explosion(glm::vec3 positon);
  ~Explosion();

  bool Update(Scene &scene, float dt);
  void Render(glm::vec3 &camera_position,glm::mat4 &viewMatrix, glm::mat4 &projectionMatrix);

  glm::vec3 speed;
private:
  float age;
  float maxAge;
  glm::vec3 rotMomentum;

  static ShaderPtr shader;
  static MeshPtr mesh;
  static TexturePtr texture;
};

#endif //PPGSO_EXPLOSION_H
