//
// Created by kovko on 29.11.2016.
//

#include "floor.h"
#include "cube_frag.h"
#include "cube_vert.h"

using namespace std;

Floor::Floor(GLfloat x, GLfloat y, GLfloat z) {
  position = glm::vec3(x, y, z);
  scale = glm::vec3(5.0, 5.0, 1);
  rotation = glm::vec3(glm::radians(90.0f), 0.0, 0.0);
  if (!shader) shader = shared_ptr<Shader>(new Shader{cube_vert, cube_frag});
  if (!texture)
    texture = shared_ptr<Texture>(new Texture{"floor.rgb", 100, 100});
  if (!mesh) mesh = shared_ptr<Mesh>(new Mesh{shader, "quad.obj"});
}

Floor::~Floor() {
}

bool Floor::Update(Scene &scene, float dt) {
  this->GenerateModelMatrix();
  return true;
}

void Floor::Render(glm::vec3 &camera_position, glm::mat4 &viewMatrix,
                   glm::mat4 &projectMatrix) {
  shader->Use();

  // use camera
  shader->SetMatrix(projectMatrix, "ProjectionMatrix");
  shader->SetMatrix(viewMatrix, "ViewMatrix");
  GLint lightPositionLoc = shader->GetUniformLocation("lightPosition");
  glUniform3f(lightPositionLoc, camera_position.x, camera_position.y,
              camera_position.z);

  // render mesh
  shader->SetMatrix(modelMatrix, "ModelMatrix");
  shader->SetTexture(texture, "Texture");
  mesh->Render();
}

bool Floor::DetectCollision(Object &other) {
  bool ret = ((int) this->position.x <= (int) other.position.x
              && (int) this->position.x + this->scale.x >= (int) other.position.x
              && (int) this->position.z <= (int) other.position.z
              && (int) this->position.z + this->scale.z >= (int) other.position.z);
  if (position.y == 0) {
    ret = ret && ((int) position.y - 1 == (int) (other.position.y));
  } else {
    ret = ret && ((int) position.y == (int) other.position.y);
  }
  return ret;
}

// shared resources
shared_ptr<Mesh> Floor::mesh;
shared_ptr<Shader> Floor::shader;
shared_ptr<Texture> Floor::texture;