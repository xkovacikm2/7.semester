//
// Created by kovko on 29.11.2016.
//

#ifndef PPGSO_FLOOR_H
#define PPGSO_FLOOR_H


#include <shader.h>
#include <mesh.h>
#include "object.h"

class Floor : public Object {
public:
  Floor(GLfloat x, GLfloat y, GLfloat z);

  ~Floor();

  bool Update(Scene &scene, float dt);

  void Render(glm::vec3 &camera_position, glm::mat4 &viewMatrix, glm::mat4 &projectMatrix);
  bool DetectCollision(Object &other);
private:
  static std::shared_ptr<Shader> shader;
  static std::shared_ptr<Mesh> mesh;
  static std::shared_ptr<Texture> texture;
};


#endif //PPGSO_FLOOR_H
