//
// Created by kovko on 15.11.2016.
//

#include <iostream>
#include <vector>
#include <map>
#include <list>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <memory>
#include "camera.h"
#include "scene.h"
#include "cube.h"
#include "floor.h"
#include "static_object.h"
#include "enemy.h"

const unsigned int SIZE = 800;

Scene scene;
// Set up the scene
void InitializeScene() {
  scene.objects.clear();

  // Create a camera
  std::shared_ptr<Camera> camera(new Camera);
  scene.camera = camera;

  scene.pistol = std::shared_ptr<Static_object>(new Static_object(glm::vec3(2, 2, 2), glm::vec3(-0.0, -0.7, 0.0), glm::vec3(0.0f, 0.0f, 0.0f), "wall.rgb", "Pistol.obj"));
  scene.objects.push_back(std::shared_ptr<Static_object>(new Static_object(glm::vec3(1, 1, 1), glm::vec3(-0.7, -0.7, 0.0), glm::vec3(glm::radians(-25.0f), 0.0, glm::radians(25.0f)), "wall.rgb", "flashlight.obj")));

  for(int i = 0; i < scene.map.size; i++){
    for(int j = 0; j < scene.map.size; j++){
      if(scene.map.map[i][j] == 1){
        scene.objects.push_back(std::shared_ptr<Cube>(new Cube(i,j)));
      }
    }
  }
  for (int i=0; i<11; i++){
    for (int j=0; j<11; j++){
      scene.objects.push_back(std::shared_ptr<Floor>(new Floor(i*5, 0, j*5)));
      scene.objects.back()->rotation.z = glm::radians(180.0f);
      scene.objects.push_back(std::shared_ptr<Floor>(new Floor(i*5 ,2, j*5)));
    }
  }
  for (int i = 0; i < 10; i++){
    auto coords = scene.map.requestSpawnPoint(3+(i%2));
    scene.objects.push_back(std::shared_ptr<Enemy>(new Enemy(coords[0]+1, coords[1]+1)));
    scene.objects.back()->position.x = coords[0];
    scene.objects.back()->position.z = coords[1];
    std::cout << "spawned at "<< coords[0] << " " << coords[1] << std::endl;
  }
}

// Keyboard press event handler
void OnKeyPress(GLFWwindow* window, int key, int, int action, int) {
  scene.keys[key] = action;

  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) glfwSetWindowShouldClose(window, true);
}

void OnClick(GLFWwindow* window, int key, int action, int){
  scene.clicks[key] = action;
}

// Mouse move event handler
void OnMouseMove(GLFWwindow*, double xpos, double ypos) {
  scene.mouse.x = xpos;
  scene.mouse.y = ypos;
}

int main() {
  // Initialize GLFW
  if (!glfwInit()) {
    std::cerr << "Failed to initialize GLFW!" << std::endl;
    return EXIT_FAILURE;
  }

  // Setup OpenGL context
  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  // Try to create a window
  auto window = glfwCreateWindow(SIZE, SIZE, "GLOOM", nullptr, nullptr);
  if (!window) {
    std::cerr << "Failed to open GLFW window, your graphics card is probably only capable of OpenGL 2.1" << std::endl;
    glfwTerminate();
    return EXIT_FAILURE;
  }

  // Finalize window setup
  glfwMakeContextCurrent(window);

  // Initialize GLEW
  glewExperimental = GL_TRUE;
  glewInit();
  if (!glewIsSupported("GL_VERSION_3_3")) {
    std::cerr << "Failed to initialize GLEW with OpenGL 3.3!" << std::endl;
    glfwTerminate();
    return EXIT_FAILURE;
  }

  // Add keyboard and mouse handlers
  glfwSetKeyCallback(window, OnKeyPress);
  glfwSetMouseButtonCallback(window, OnClick);
  glfwSetCursorPosCallback(window, OnMouseMove);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // Initialize OpenGL state
  // Enable Z-buffer
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

//  // Enable polygon culling
//  glEnable(GL_CULL_FACE);
//  glFrontFace(GL_CCW);
//  glCullFace(GL_BACK);

  InitializeScene();

  // Track time
  float time = (float)glfwGetTime();

  // Main execution loop
  while (!glfwWindowShouldClose(window)) {
    // Compute time delta
    float dt = (float)glfwGetTime() - time;
    time = (float)glfwGetTime();

    // Set black background
    glClearColor(0.5f,0.5f,0.5f,0.0f);
    // Clear depth and color buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Update and render all objects
    scene.Update(dt);
    scene.Render();

    // Display result
    glfwSwapBuffers(window);
    glfwPollEvents();

    if(scene.game_over){
      std::cout << scene.message << std::endl;
      break;
    }
  }

  // Clean up
  glfwTerminate();

  return EXIT_SUCCESS;
}