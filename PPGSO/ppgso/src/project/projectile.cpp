#include "scene.h"
#include "projectile.h"

#include "cube_frag.h"
#include "cube_vert.h"
#include "explosion.h"

Projectile::Projectile(glm::vec3 position, glm::vec3 direction)
  : maxLife(1) {
  this->position = position;
  this->life = 0;
  this->direction = direction;
  this->alive = true;
  // Set default speed
  speed = 0.9;
}

Projectile::~Projectile() {
}

bool Projectile::Update(Scene &scene, float dt) {
  while (alive) {
    this->life += dt;
    this->position += this->direction * this->speed;
    for (auto &object : scene.objects) {
      if (object.get() == this) {
        continue;
      }

      if (object->DetectCollision(*this)) {
        alive = object->alive = false;
        break;
      }
    }
    if (this->life > this->maxLife) {
      this->alive = false;
    }
  }
  scene.objects.push_back(
    std::shared_ptr<Explosion>(new Explosion(this->position)));
  return false;
}

void Projectile::Render(glm::vec3 &camera_position,glm::mat4 &viewMatrix, glm::mat4 &projectionMatrix) {
}

