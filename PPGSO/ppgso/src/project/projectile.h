#ifndef PPGSO_PROJECTILE_H
#define PPGSO_PROJECTILE_H

#include "shader.h"
#include "mesh.h"
#include "object.h"

class Projectile : public Object {
public:
  Projectile(glm::vec3 position, glm::vec3 direction);
  ~Projectile();

  bool Update(Scene &scene, float dt) ;
  void Render(glm::vec3 &camera_position,glm::mat4 &viewMatrix, glm::mat4 &projectionMatrix) ;

  void Destroy();
private:
  GLfloat speed;
  glm::vec3 direction;
  const GLfloat maxLife;
  GLfloat life;
};

#endif //PPGSO_PROJECTILE_H
