#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "scene.h"

Scene::Scene() {
  game_over=false;
  message="";
  kill_count = 0;
}

Scene::~Scene() {
}

void Scene::Update(float time) {
  if (kill_count == 10){
    message = "You are victorious";
    game_over = true;
    return;
  }

  if (keys[GLFW_KEY_W])
    camera->ProcessKeyboard(*this, Camera_Movement::FORWARD, time);
  if (keys[GLFW_KEY_S])
    camera->ProcessKeyboard(*this, Camera_Movement::BACKWARD, time);
  if (keys[GLFW_KEY_A])
    camera->ProcessKeyboard(*this, Camera_Movement::LEFT, time);
  if (keys[GLFW_KEY_D])
    camera->ProcessKeyboard(*this, Camera_Movement::RIGHT, time);
  if (clicks[GLFW_MOUSE_BUTTON_LEFT]){
    pistol->Process_LMB(*this);
  }

  camera->ProcessMouseMovement(mouse.x, mouse.y);


  pistol->Update(*this, time);

  // Use iterator to update all objects so we can remove while iterating
  auto i = std::begin(objects);

  while (i != std::end(objects)) {
    // Update and remove from list if needed
    auto obj = i->get();
    if (!obj->Update(*this, time))
      i = objects.erase(i); // NOTE: no need to call destructors as we store shared pointers in the scene
    else
      ++i;
  }
}

void Scene::Render() {
  auto viewMatrix = camera->GetViewMatrix();
  auto projectionMatrix = camera->projectionMatrix;

  pistol->Render(camera->Position, viewMatrix, projectionMatrix);

  // Simply render all objects
  for (auto obj : objects )
    obj->Render(camera->Position, viewMatrix, projectionMatrix);
}

