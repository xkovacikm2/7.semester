#ifndef _PPGSO_SCENE_H
#define _PPGSO_SCENE_H

#include <memory>
#include <map>
#include <list>

#include "camera.h"
#include "object.h"
#include "MapGenerator.h"
#include "static_object.h"

class Camera;
// Simple object that contains all scene related data
// Object pointers are stored in a list of objects
// Keyboard and Mouse states are stored in a map and struct
class Scene {
public:
  Scene();

  ~Scene();

  // Animate all objects in scene
  void Update(float time);

  // Render all objects in scene
  void Render();

  std::shared_ptr<Camera> camera;
  std::list<std::shared_ptr<Object>> objects;
  std::shared_ptr<Static_object> pistol;
  std::map<int, bool> keys;
  std::map<int, bool> clicks;
  struct {
    GLfloat x, y;
  } mouse;

  MapGenerator map;
  std::string message;
  bool game_over;
  int kill_count;
};

#endif // _PPGSO_SCENE_H
