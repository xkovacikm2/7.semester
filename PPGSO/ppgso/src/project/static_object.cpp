//
// Created by kovko on 30.11.2016.
//

#include "projectile.h"
#include "flash_vert.h"
#include "flash_frag.h"
#include "static_object.h"
#include "scene.h"

using namespace std;

Static_object::Static_object(glm::vec3 scale, glm::vec3 position, glm::vec3 rotation, std::string texture_path, std::string mesh_path)
  : animation_max_duration(0.5f){
  this->position = position;
  this->scale = scale;
  this->rotation = rotation;
  shader = shared_ptr<Shader>(new Shader{flash_vert, flash_frag});
  texture = shared_ptr<Texture>(new Texture{texture_path, 100, 100});
  mesh = shared_ptr<Mesh>(new Mesh{shader, mesh_path});
  animating = false;
  animation_current_duration = 0.0f;
}

Static_object::~Static_object() {
}

bool Static_object::Update(Scene &scene, float dt) {
  if(animating){
    rotation.x = glm::radians(-5.0f * animation_max_duration/animation_current_duration);
    animation_current_duration += dt;
    if (animation_current_duration > animation_max_duration){
      animating = false;
    }
  }
  this->GenerateModelMatrix();
  return true;
}

void Static_object::Render(glm::vec3 &camera_position,glm::mat4 &viewMatrix, glm::mat4 &projectMatrix) {
  shader->Use();

  // use camera
  shader->SetMatrix(projectMatrix, "ProjectionMatrix");
  shader->SetMatrix(viewMatrix, "ViewMatrix");

  // render mesh
  shader->SetMatrix(modelMatrix, "ModelMatrix");
  shader->SetTexture(texture, "Texture");
  mesh->Render();
}

void Static_object::Process_LMB(Scene &scene) {
  if (!this->animating){
    scene.objects.push_back(std::shared_ptr<Projectile>(new Projectile(scene.camera->Position, scene.camera->Front)));
    animating = true;
    animation_current_duration = 0.0f;
  }
}
