//
// Created by kovko on 30.11.2016.
//

#ifndef PPGSO_FLASH_H
#define PPGSO_FLASH_H


#include <shader.h>
#include <mesh.h>
#include <GL/gl.h>
#include "object.h"

class Static_object : public Object {
public:
  Static_object(glm::vec3 scale, glm::vec3 position, glm::vec3 rotation, std::string texture_path, std::string mesh_path);

  ~Static_object();

  bool Update(Scene &scene, float dt);

  void Render(glm::vec3 &camera_position,glm::mat4 &viewMatrix, glm::mat4 &projectMatrix);

  void Process_LMB(Scene &scene);
private:
  bool animating;
  const GLfloat animation_max_duration;
  GLfloat animation_current_duration;
  std::shared_ptr<Shader> shader;
  std::shared_ptr<Mesh> mesh;
  std::shared_ptr<Texture> texture;
};


#endif //PPGSO_FLASH_H
