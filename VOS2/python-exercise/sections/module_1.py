# Section 1. Very simple exercises
#
# This selection of exercises is intended for developers
# to get a basic understanding of logical operators and loops in Python
#
import functools
import re


# 1. Max of two numbers.
def max_num(a, b):
  return a if a > b else b


# 2. Max of three numbers.
def max_of_three(a, b, c):
  return max(a, b, c)


# 3. Calculates the length of a string.
def str_len(string):
  return len(string)


# 4. Returns whether the passed letter is a vowel.
def is_vowel(letter):
  return letter in "aeiouy"


# 5. Translates an English frase into `Robbers language`.
# Sample:
#
#   This is fun
#   Tothohisos isos fofunon
#
def translate(string):
  fofunon = []
  for c in string:
    fofunon.append(c)
    if c.isalpha() and not is_vowel(c):
      fofunon.append('o')
      fofunon.append(c.lower())
  return ''.join(fofunon)


# 6. Sum.
# Sums all the numbers in a list.
def sum(items):
  return functools.reduce(lambda x, y: x + y, items)


# 6.1. Multiply.
# Multiplies all the items in a list.
def multiply(items):
  return functools.reduce(lambda x, y: x * y, items)


# 7. Reverse.
# Reverses a string.
# 'I am testing' -> 'gnitset ma I'
def reverse(string):
  return string[::-1]


# 8. Is palindrome.
# Checks whether a string is palindrome.
# 'radar' > reversed : 'radar'
def is_palindrome(string):
  return string == reverse(string)


# 9. Is member.
# Checks whether a value x is contained in a group of values.
#   1 -> [ 2, 1, 0 ] : True
def is_member(x, group):
  return x in group


# 10. Overlapping.
# Checks whether two lists have at least one number in common
def overlapping(a, b):
  return not set(a).isdisjoint(set(b))


# 11. Generate n chars.
# Generates `n` number of characters of the given one.
#
#   generate_n_chars( 5, 'n' )
#   -> nnnnn
#
def generate_n_chars(times, char):
  return char * times


# 12. Historigram.
# Takes a list of integers and prints a historigram of it.
#   historigram( [ 1, 2, 3 ] ) ->
#       *
#       **
#       ***
#
def historigram(items):
  for i in items:
    print('*' * i)


# 13. Max in list.
# Gets the larges number in a list of numbers.
def max_in_list(list):
  return max(list)


# 14. Map words to numbers.
# Gets a list of words and returns a list of integers
# representing the length of each word.
#
#   [ 'one', 'two', 'three' ] -> [ 3, 3, 5 ]
#
def map_words(words):
  return list(map(str_len, words))


# 15. Find longest wors.
# Receives a list of words and returns the length
# of the longest one.
#
#   [ 'one', 'two', 'three', 'four' ] -> 5
#
def longest_word(words):
  return max(map_words(words))


# 16. Filter long words.
# Receives a list of words and an integer `n` and returns
# a list of the words that are longer than n.
def filter_long_words(words, x):
  return list(filter(lambda n: len(n) >= x, words))


# 17. Version of palindrome that ignores punctuation, capitalization and
# spaces, so that a larger range of frases can be clasified as palindromes.
#
#   ( "Dammit, I'm mad!" ) -> is palindrome
#
def is_palindrome_advanced(string):
  return is_palindrome(re.sub('[^a-zA-Z]', '', string).lower())


# 18. Is pangram.
# Checks whether a phrase is pangram, that is, if
# it contains all the letters of the alphabet.
def is_pangram(phrase):
  return sorted(set(re.sub('[^a-z]', '', phrase.lower()))) == list(map(chr, range(ord('a'), ord('z') + 1)))


# 19. 99 Bottles of beer.
# 99 Bottles of beer is a traditional song in the United States and Canada.
# It has a very repetitive lyrics and it is popular to sing it in very long trips.
# The lyrics of the song are as follows.
#
#   99 bottles of beer in the wall, 99 bottles of beer.
#   Take one down, pass it arrown, 98 bottles of beer.
#
# The song is repeated having one less bottle each time until there are no more
# bottles to count.
#
def sing_99_bottles_of_beer():
  for i in range(1, 99):
    print("%d bottles of beer in the wall, %d bottles of beer." % (100 - i, 100 - i))
    print("Take one down, pass it arrown, %d bottles of beer." % (100 - i - 1))


# 20. Note: exercise number 20 is the same as exercise # 30


# 21. Character frequency.
# Counts how many characters of the same letter there are in
# a string.
#
#   ( 'aabbccddddd' ) -> { 'a': 2, 'b': 2, 'c': 2, d: 5 }
#
def char_freq(string):
  res = {}
  for c in string:
    if not c in res:
      res[c] = 0
    res[c] += 1
  return res


# 22. ROT-13: Encrypt.
# Encrypts a string in ROT-13.
#
#   rot_13_encrypt( 'Caesar cipher? I much prefer Caesar salad!' ) ->
#   Pnrfne pvcure? V zhpu cersre Pnrfne fnynq!
#
def rot_13_encrypt(string, key = 13):
  ciphered = ''
  for symbol in string:
    num = ord(symbol)
    if symbol.isalpha():
      num += 13
    if symbol.isupper():
      if num > ord('Z'):
        num -= 26
      elif num < ord('A'):
        num += 26
    elif symbol.islower():
      if num > ord('z'):
        num -= 26
      elif num < ord('a'):
        num += 26
    ciphered += chr(num)
  return ciphered

# 22.1 ROT-13: Decrypt.
#
#   rot_13_decrypt( 'Pnrfne pvcure? V zhpu cersre Pnrfne fnynq!' ) ->
#   Caesar cipher? I much prefer Caesar salad!
#
# Since we're dealing with offset 13 it means that decrypting a string
# can be accomplished with the encrypt function given that the alphabet contains
# 26 letters.
def rot_13_decrypt(string):
  return rot_13_encrypt(string, -13)


# 23. Correct.
# Takes a string and sees that 1) two or more occurences of a space
# are compressed into one. 2) Adds a space betweet a letter and a period
# if they have not space.
#
#   correct( 'This   is  very funny  and    cool.Indeed!' )
#   -> This is very funny and cool. Indeed!
#
def correct(string):
  return re.sub(' +', ' ', re.sub('\.', '. ', string)).strip()

# 24. Make third Form.
# Takes a singular verb and makes it third person.
#
#   ( 'run' ) -> 'runs'
#   ( 'Brush' ) -> 'brushes'
#
def make_3d_form(verb):
  if verb.endswith('y'):
    return re.sub('y$', 'ies', verb)
  elif verb.endswith(('o', 'ch', 's', 'sh', 'x', 'z')):
    return verb + 'es'
  else:
    return verb + 's'


# 25. Make `ing` form.
# Given an infinite verb this function returns the
# present participle of it.
#
#   ( 'go' ) -> 'going'
#   ( 'sleep' ) -> 'sleep'
#
def make_ing_form(verb):
  if verb.endswith('ie'):
    verb = re.sub('ie$', 'y', verb)
  elif verb.endswith('e'):
    verb = re.sub('e$', '', verb)
  elif verb.endswith('n'):
    verb = re.sub('n$', 'nn', verb)
  return verb + 'ing'
